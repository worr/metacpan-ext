var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-11164924-4']);
_gaq.push(['_trackPageview']);

(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = 'https://ssl.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

var matchobjs = [
	{
		'type': 'author',
		'regexp': /^https?:\/\/search.cpan.org\/~(\w+)\/?$/,
		'uri': "https://metacpan.org/author/",
		'onmatch': function(link) {
			link.href = this.uri + link.href.match(this.regexp)[1];
		}
	},
	{
		'type': 'release',
		'regexp': /^https?:\/\/search.cpan.org\/~\w+\/([a-zA-Z0-9\-\._%]+)\/?$/,
		'uri': "https://metacpan.org/release/",
		'onmatch': function(link) {
			var release = link.href.match(this.regexp)[1];
			var idx = release.lastIndexOf('-');
			if (idx == -1) {
				idx = release.length;
			}

			link.href = this.uri + release.substring(0, idx);
		}
	},
	{
		'type': 'release',
		'regexp': /^https?:\/\/search.cpan.org\/dist\/([a-zA-Z0-9\-\._%]+)\/?$/,
		'uri': "https://metacpan.org/release/",
		'onmatch': function(link) {
			var release = link.href.match(this.regexp)[1];
			var idx = release.lastIndexOf('-');
			if (idx == -1) {
				idx = release.length;
			}

			link.href = this.uri + release.substring(0, idx);
		}
	},
	{
		'type': 'module',
		'regexp': /^https?:\/\/search.cpan.org\/~\w+\/[a-zA-Z0-9\-\._%]+\/lib\/(.+)\.(pm|pod)\/?$/,
		'uri': "https://metacpan.org/module/",
		'onmatch': function(link) {
			var module = link.href.match(this.regexp)[1];
			module = module.replace(/\//g, '::');

			link.href = this.uri + module;
		}
	},
	{
		'type': 'module',
		'regexp': /^https?:\/\/search.cpan.org\/dist\/[a-zA-Z0-9\-\._%]+\/lib\/(.+)\.(pm|pod)\/?$/,
		'uri': "https://metacpan.org/module/",
		'onmatch': function(link) {
			var module = link.href.match(this.regexp)[1];
			module = module.replace(/\//g, '::');

			link.href = this.uri + module;
		}
	},
	{
		'type': 'module',
		'regexp': /^https?:\/\/search.cpan.org\/perldoc\?([a-zA-Z0-9%]+)\/?$/,
		'uri': "https://metacpan.org/module/",
		'onmatch': function(link) {
			var module = link.href.match(this.regexp)[1];

			link.href = this.uri + module;
		}
	}
];

// Expects an object with an href param
var match = function(link) {
	matchobjs.forEach(function(element, index, array) {
		if (element.regexp.test(link.href)) {
			element.onmatch(link);
		}
	});
};

chrome.extension.onRequest.addListener(function(request, sender, sendResponse) {
	_gaq.push(['_trackEvent', sender.id, 'live_replace']);
	match(request);
	sendResponse(request);
});


// Create menu entry for searching selected text on metacpan
chrome.contextMenus.create({
	'title': 'Find on metacpan',
	'contexts': [ 'selection' ],
	'onclick': function(info, tab) {
		_gaq.push(['_trackEvent', info.menuItemId, 'find_on_metacpan']);
		chrome.tabs.create({
			'url': 'https://metacpan.org/search?q=' + info.selectionText
		});
	}
});

// Now let's create one for search.cpan.org
chrome.contextMenus.create({
	'title': 'Open on metacpan',
	'contexts': [ 'page' ],
	'documentUrlPatterns': [ 'http://search.cpan.org/*' ],
	'onclick': function(info, tab) {
		var req = { 'href': tab.url };
		var targetRegexp = /^https?:\/\/search.cpan.org\/.*\/([\._\-a-zA-Z0-9]+)$/;
		_gaq.push(['_trackEvent', info.menuItemId, 'open_on_metacpan']);

		match(req);
		if (targetRegexp.test(req.href)) {
			req.href = 'https://metacpan.org/search?q=' + req.href.match(targetRegexp)[1];
		}

		chrome.tabs.update(tab.id, { 'url': req.href });
	}
});
