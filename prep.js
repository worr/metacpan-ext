var links = document.querySelectorAll("a");
var targetRegexp = /^https?:\/\/search.cpan.org/;
for (var i = 0; i < links.length; i++) {
	if (targetRegexp.test(links[i].href)) {
		chrome.extension.sendRequest({ 'idx': i, 'href': links[i].href }, function(response) {
			links[response.idx].href = response.href;
		});
	}
}
